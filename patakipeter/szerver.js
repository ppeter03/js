const net = require('net'); 
var server = net.createServer(); 
server.on("connection", (socket) => { 
  console.log("new client connection is made", socket.remoteAddress + ":" + socket.remotePort); 
  socket.on("data", (data) => { 
    console.log(data.toString()); 
  });
  process.stdin.on("data", (send) => {
socket.write(send);
}); 
  socket.once("close", () => { 
    console.log("A kliennsel való csatlakozás megszűnt!"); 
  }); 
  socket.on("error", (err) => { 
    console.log("A kliennsel való kapcsolat megszakadt") 
  }); 
  socket.write('SZERVER: A csatlakozás a klienshez sikeresen megtörtént.<br>'); 
}); 
server.on('error', (e) => { 
  if (e.code === 'EADDRINUSE') { 
    console.log('Az IP cím használatban van. Kérem próbálkozzon újra késöbb'); 
    setTimeout(() => { 
      server.close(); 
      server.listen(PORT, HOST); 
    }, 1000); 
  } 
  else { 
    console.log("A szerver csatlakozása sikertelen!") 
  } 
}); 
server.listen(8888, () => { 
  console.log('opened server on %j', server.address().port); 
});