const net = require('net'); 
const readline = require('readline'); 
const rl = readline.createInterface({ 
  input: process.stdin, 
  output: process.stdout 
}); 
const client = net.createConnection({ port: 8888 }, () => { 
process.stdout.write('KLIENS: Sikerült csatlakozni a szerverre');
//
  client.write('KLIENS: A kliens sikeresen csatlakozott a szerverhez'); 
}); 
client.on('data', (data) => { 
  process.stdout.write(data.toString());
  //client.end(); 
}); 
client.on('end', () => { 
  console.log('A kliens lecsatlakozott a szerverről.'); 
}) 
rl.on('line', (input) => { 
  client.write('KLIENS: ${input}'); 
});